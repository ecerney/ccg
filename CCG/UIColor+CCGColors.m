//
//  UIColor+CCGColors.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "UIColor+CCGColors.h"

@implementation UIColor (CCGColors)

+(UIColor *)regionBackgroundColor {
    // translucent color so background mat shows
    // for debug purposes
    return [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.3];
    
    // for production, don't show the color
    //return [UIColor clearColor];
}

@end
