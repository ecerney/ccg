//
//  RWCombatOverlay.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWCombatOverlay.h"
#import "UIColor+CCGColors.h"
#import "RWCard.h"
#import "RWGameScene.h"
#import "RWPlayerTracker.h"
#import "RWPlayer.h"

@interface RWCombatOverlay ()

@property (weak, nonatomic, readonly) RWGameScene *gameScene;
@property (weak, nonatomic) RWCard *originalAttacker;
@property (weak, nonatomic) RWCard *originalDefender;

@property (strong, nonatomic) SKSpriteNode *attacker;
@property (strong, nonatomic) SKSpriteNode *defender;

@property (assign, nonatomic) NSInteger step;

@end

@implementation RWCombatOverlay

- (id)initWithColor:(UIColor *)color size:(CGSize)size {
    self = [super initWithColor:[UIColor regionBackgroundColor] size:size];
    if (self) {
        self.zPosition = -20;
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (RWGameScene *)gameScene {
    return (RWGameScene *)self.scene;
}

#pragma mark - Combat Steps

- (void)showFightWithAttacker:(RWCard *)attacker andDefender:(RWCard *)defender andRotation:(CGFloat)rotation {
    // careful - defender might be nil (no defender or pass)
    self.step = 1;
    self.zPosition = 20;
    self.zRotation = rotation;
    
    self.originalAttacker = attacker;
    self.originalDefender = defender;
    
    self.attacker = [SKSpriteNode spriteNodeWithImageNamed:attacker.largeCardFileName];
    self.attacker.position = CGPointMake(-171, 0);
    [self addChild:self.attacker];
    
    if (defender) {
        self.defender = [SKSpriteNode spriteNodeWithImageNamed:defender.largeCardFileName];
        self.defender.position = CGPointMake(171, 0);
        [self addChild:self.defender];
    } else {
        // no defender, show something to represent the player
        SKSpriteNode *player;
        if ([self.gameScene.defendingPlayer.name isEqualToString:@"Druid"]) {
            player = [SKSpriteNode spriteNodeWithImageNamed:@"player1_druid.png"];
        } else {
            player = [SKSpriteNode spriteNodeWithImageNamed:@"player2_beastmaster.png"];
        }
        player.position = CGPointMake(171, 60);
        [self addChild:player];
        
        SKSpriteNode *heart = [SKSpriteNode spriteNodeWithImageNamed:@"icon_heart.png"];
        heart.position = CGPointMake(144, 10);
        [self addChild:heart];
        
        SKLabelNode *healthLabel = [SKLabelNode labelNodeWithFontNamed:@"OpenSans-Bold"];
        healthLabel.name = @"health";
        healthLabel.fontSize = 36;
        healthLabel.fontColor = [UIColor blackColor];
        healthLabel.position = CGPointMake(190, -5);
        NSInteger health = [self.gameScene.playerTracker healthForPlayer:self.gameScene.defendingPlayer];
        healthLabel.text = [NSString stringWithFormat:@"%ld", (long)health];
        [self addChild:healthLabel];
    }
    
    // damage overlay
    // add damage overlay, mimicing what will show on the actual card
    
    SKLabelNode *attackerDamageOverlay = [SKLabelNode labelNodeWithFontNamed:@"OpenSans-Bold"];
    attackerDamageOverlay.fontSize = 24;
    attackerDamageOverlay.name = @"AttackerDamageOverlay";
    attackerDamageOverlay.fontColor = [UIColor colorWithRed:0.47 green:0.0 blue:0.0 alpha:1.0];
    attackerDamageOverlay.text = [NSString stringWithFormat:@"-%ld", (long)self.originalAttacker.damage];
    if (self.originalAttacker.damage == 0) {
        attackerDamageOverlay.hidden = YES;
    }
    attackerDamageOverlay.position = CGPointMake(-105, 121);
    [self addChild:attackerDamageOverlay];
    
    if (self.originalDefender) {
        SKLabelNode *defenderDamageOverlay = [SKLabelNode labelNodeWithFontNamed:@"OpenSans-Bold"];
        defenderDamageOverlay.fontSize = 24;
        defenderDamageOverlay.name = @"DefenderDamageOverlay";
        defenderDamageOverlay.fontColor = [UIColor colorWithRed:0.47 green:0.0 blue:0.0 alpha:1.0];
        defenderDamageOverlay.text = [NSString stringWithFormat:@"-%ld", (long)self.originalDefender.damage];
        if (self.originalDefender.damage == 0) {
            defenderDamageOverlay.hidden = YES;
        }
        defenderDamageOverlay.position = CGPointMake(236, 121);
        [self addChild:defenderDamageOverlay];
    }
    
    SKAction *delay = [SKAction waitForDuration:0.1];
    SKAction *next = [SKAction performSelector:@selector(assignDamage) onTarget:self];
    SKAction *waitRunNext = [SKAction sequence:@[delay, next]];
    [self runAction:waitRunNext withKey:@"delay"];
}

- (void)assignDamage {
    self.step = 2;
    SKAction *driftUp = [SKAction moveByX:0 y:50 duration:0.5];
    SKAction *fadeOut = [SKAction fadeOutWithDuration:0.5];
    SKAction *fadeUp = [SKAction group:@[driftUp, fadeOut]];
    
    if (self.originalDefender) {
        // attacker loss of health, like in MMO's
        SKLabelNode *attackerDamage = [SKLabelNode labelNodeWithFontNamed:@"OpenSans-Regular"];
        attackerDamage.fontSize = 40;
        attackerDamage.fontColor = [UIColor colorWithRed:0.47 green:0.0 blue:0.0 alpha:1.0];
        attackerDamage.text = [NSString stringWithFormat:@"-%ld", (long)self.originalDefender.attackValue];
        attackerDamage.position = CGPointMake(-171, 250);
        [self addChild:attackerDamage];
        [attackerDamage runAction:fadeUp];
        
        // update atacker damage overlay
        SKLabelNode *attackerDamageOverlay = (SKLabelNode *)[self childNodeWithName:@"AttackerDamageOverlay"];
        NSInteger attackerTotalDamage = self.originalAttacker.damage + self.originalDefender.attackValue;
        attackerDamageOverlay.text = [NSString stringWithFormat:@"-%ld", (long)attackerTotalDamage];
        if (attackerTotalDamage > 0) {
            attackerDamageOverlay.hidden = NO;
        }
    }
    
    // defender loss of health, like in MMO's
    SKLabelNode *defenderDamage = [SKLabelNode labelNodeWithFontNamed:@"OpenSans-Regular"];
    defenderDamage.fontSize = 40;
    defenderDamage.fontColor = [UIColor colorWithRed:0.47 green:0.0 blue:0.0 alpha:1.0];
    defenderDamage.text = [NSString stringWithFormat:@"-%ld", (long)self.originalAttacker.attackValue];
    defenderDamage.position = CGPointMake(171, 250);
    [defenderDamage runAction:fadeUp];
    [self addChild:defenderDamage];
    
    // update defender damage overlay
    if (self.originalDefender) {
        SKLabelNode *defenderDamageOverlay = (SKLabelNode *)[self childNodeWithName:@"DefenderDamageOverlay"];
        NSInteger defenderTotalDamage = self.originalDefender.damage + self.originalAttacker.attackValue;
        defenderDamageOverlay.text = [NSString stringWithFormat:@"-%ld", (long)defenderTotalDamage];
        if (defenderTotalDamage > 0) {
            defenderDamageOverlay.hidden = NO;
        }
    }
    
    if (self.defender) {
        [self runAction:[SKAction playSoundFileNamed:@"two-swords.wav" waitForCompletion:NO]];
    } else {
        [self runAction:[SKAction playSoundFileNamed:@"one-sword.wav" waitForCompletion:NO]];
        SKLabelNode *healthLabel = (SKLabelNode *)[self childNodeWithName:@"health"];
        NSInteger health = [self.gameScene.playerTracker healthForPlayer:self.gameScene.defendingPlayer]
                            - self.originalAttacker.attackValue;
        healthLabel.text = [NSString stringWithFormat:@"%ld", (long)health];
    }

    [self removeActionForKey:@"delay"];
    SKAction *delay = [SKAction waitForDuration:1.0];
    SKAction *next = [SKAction performSelector:@selector(didCreaturesDie) onTarget:self];
    SKAction *waitRunNext = [SKAction sequence:@[delay, next]];
    [self runAction:waitRunNext withKey:@"delay"];
}

- (void)didCreaturesDie {
    self.step = 3;
    NSInteger deadCount = 0;
    
    if (!self.originalDefender) {
        //[self.playerTracker doDamage:self.attackingCard.attackValue toPlayer:self.defendingPlayer];
    } else {
        [self.originalDefender doDamage:self.originalAttacker.attackValue];
        [self.originalAttacker doDamage:self.originalDefender.attackValue];
        
        if (self.originalAttacker.damage >= self.originalAttacker.defenseValue) {
            //[self.originalAttacker discard];
            SKSpriteNode *deadX = [SKSpriteNode spriteNodeWithImageNamed:@"deadCreature.png"];
            deadX.position = self.attacker.position;
            [self addChild:deadX];
            deadCount++;
        }
        
        if (self.originalDefender.damage >= self.originalDefender.defenseValue) {
            //[self.originalDefender discard];
            SKSpriteNode *deadX = [SKSpriteNode spriteNodeWithImageNamed:@"deadCreature.png"];
            deadX.position = self.defender.position;
            [self addChild:deadX];
            deadCount++;
        }
        SKAction *playSound = [SKAction playSoundFileNamed:@"thump.wav" waitForCompletion:YES];
        [self runAction:[SKAction repeatAction:playSound count:deadCount]];
    }
    
    [self removeActionForKey:@"delay"];
    
    if (deadCount > 0) {
        SKAction *delay = [SKAction waitForDuration:0.5];
        SKAction *next = [SKAction performSelector:@selector(dismissDisplay) onTarget:self];
        SKAction *waitRunNext = [SKAction sequence:@[delay, next]];
        [self runAction:waitRunNext withKey:@"delay"];
    } else {
        [self nextStep];
    }
    
}

- (void)dismissDisplay {
    self.attacker = nil;
    self.defender = nil;
    self.zPosition = -20;
    [self removeAllChildren];
    [(RWGameScene *)self.scene endFight];
}

- (void)nextStep {
    //self.step += 1;
    switch (self.step) {
        case 1:
            [self removeActionForKey:@"delay"];
            [self assignDamage];
            break;
        case 2:
            [self removeActionForKey:@"delay"];
            [self didCreaturesDie];
            break;
        case 3:
            [self removeActionForKey:@"delay"];
            [self dismissDisplay];
            break;
    }
    
}

#pragma mark - Touch Handlers

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self removeActionForKey:@"delay"];
    [self nextStep];
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

@end
