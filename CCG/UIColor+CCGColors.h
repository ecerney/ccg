//
//  UIColor+CCGColors.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CCGColors)

+ (UIColor *)regionBackgroundColor;

@end
