//
//  RWCreditScene.m
//  CCG
//
//  Created by Brian Broom on 2/28/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

@import AVFoundation;

#import "RWCreditScene.h"
#import "DSMultilineLabelNode.h"
#import "RWMenuScene.h"

@interface RWCreditScene ()

@property (strong, nonatomic) AVAudioPlayer *musicPlayer;
@property (assign, nonatomic) NSInteger counter;
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) NSMutableArray *links;
@property (strong, nonatomic) DSMultilineLabelNode *label;

@end

@implementation RWCreditScene

- (instancetype)initWithSize:(CGSize)size {
    self = [super initWithSize:size];
    if (self) {
        
        // play background music
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Vulcan" ofType:@"mp3"];
        NSURL *musicFile = [[NSURL alloc] initFileURLWithPath:path];
        NSError *error = nil;
        _musicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile  error:&error];
        _musicPlayer.numberOfLoops = -1; // negative value repeats indefinitely
        [_musicPlayer prepareToPlay];
        [_musicPlayer play];
        
        // background image
        SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"bg_blank.png"];
        bg.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        [self addChild:bg];
        
        // button to return to menu
        SKSpriteNode *menuButton = [SKSpriteNode spriteNodeWithImageNamed:@"MenuButton.png"];
        menuButton.name = @"menu";
        menuButton.position = CGPointMake(CGRectGetMidX(self.frame), 50);
        [self addChild:menuButton];
        
        _messages = [NSMutableArray new];
        _links = [NSMutableArray new];
        
        // messages to be displayed
        [_messages addObject:@"Special thanks to"];
        [_links addObject:@""];
        
        [_messages addObject:@"Artwork by Vicki Wenderlich"];
        [_links addObject:@"http://www.vickiwenderlich.com"];
        
        [_messages addObject:@"Incompetech.com for music"];
        [_links addObject:@"http://incompetech.com/"];
        
        [_messages addObject:@"Freesound.org for sound effects"];
        [_links addObject:@"http://www.freesound.org/"];
        
        [_messages addObject:@"\"Black Vortex\" Kevin MacLeod (incompetech.com) "
                       "Licensed under Creative Commons: By Attribution 3.0 "
                       "http://creativecommons.org/licenses/by/3.0/"];
        [_links addObject:@"http://incompetech.com/music/royalty-free/index.html?isrc=USUAN1300037"];
        
        [_messages addObject:@"\"Take a Chance\" Kevin MacLeod (incompetech.com) "
                        "Licensed under Creative Commons: By Attribution 3.0 "
                        "http://creativecommons.org/licenses/by/3.0/"];
        [_links addObject:@"http://incompetech.com/music/royalty-free/index.html?isrc=USUAN1300024"];
        
        [_messages addObject:@"\"Pippin the Hunchback\" Kevin MacLeod (incompetech.com) "
                            "Licensed under Creative Commons: By Attribution 3.0 "
                            "http://creativecommons.org/licenses/by/3.0/"];
        [_links addObject:@"http://incompetech.com/music/royalty-free/index.html?isrc=USUAN1400005"];
        
        [_messages addObject:@"\"Vulcan\" Kevin MacLeod (incompetech.com) "
                            "Licensed under Creative Commons: By Attribution 3.0 "
                            "http://creativecommons.org/licenses/by/3.0/"];
        [_links addObject:@"http://incompetech.com/music/royalty-free/index.html?isrc=USUAN1100525"];
        
        [_messages addObject:@"Click sound by TicTacShutUp"];
        [_links addObject:@"http://www.freesound.org/people/TicTacShutUp/"];
        
        [_messages addObject:@"Card draw sound by gynation"];
        [_links addObject:@"http://www.freesound.org/people/gynation/sounds/82379/"];
        
        [_messages addObject:@"Card flip sound by Koops"];
        [_links addObject:@"http://www.freesound.org/people/Koops/sounds/20241/"];
        
        [_messages addObject:@"Sword Clang 1 by Erdie"];
        [_links addObject:@"http://www.freesound.org/people/Erdie/sounds/27826/"];
        
        [_messages addObject:@"Sword Clang 2 by Erdie"];
        [_links addObject:@"http://www.freesound.org/people/Erdie/sounds/27858/"];
        
        [_messages addObject:@"Creature Death Sound by numar"];
        [_links addObject:@"http://www.freesound.org/people/numar/sounds/118401/"];
        
        [_messages addObject:@"Victory Sound by primordiality"];
        [_links addObject:@"http://www.freesound.org/people/primordiality/sounds/78824/"];
        
        [_messages addObject:@"Phase change clink by DJ Burnham"];
        [_links addObject:@"http://www.freesound.org/people/DJ%20Burnham/sounds/76812/"];
        
        [_messages addObject:@"Turn Change clink by Agonda"];
        [_links addObject:@"http://www.freesound.org/people/Agonda/sounds/210581/"];
        
        [_messages addObject:@"Death Ray zap by themfish"];
        [_links addObject:@"http://www.freesound.org/people/themfish/sounds/34205/"];
        
        [_messages addObject:@"Energy Activation by fins"];
        [_links addObject:@"http://www.freesound.org/people/fins/sounds/191592/"];
        
        [_messages addObject:@"Open Sans font from Google Fonts"];
        [_links addObject:@"http://www.google.com/fonts/specimen/Open+Sans"];
        
        [_messages addObject:@"Onramp font by Michael Spitz"];
        [_links addObject:@"http://www.losttype.com/font/?name=ONRAMP,"];
        
        [_messages addObject:@"Text overlay code uses DSMultilineLabelNode by Chris Allwein"];
        [_links addObject:@"https://github.com/downrightsimple/DSMultilineLabelNode"];
        
        _counter = 0;
        
        // message node
        _label = [DSMultilineLabelNode labelNodeWithFontNamed:@"OpenSans"];
        _label.fontSize = 40;
        _label.text = _messages[_counter];
        _label.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        _label.alpha = 0;
        [self addChild:_label];
        
        SKAction *fadeIn = [SKAction fadeInWithDuration:0.5];
        SKAction *delay = [SKAction waitForDuration:2.0];
        SKAction *fadeOut = [SKAction fadeOutWithDuration:0.5];
        SKAction *nextMessage = [SKAction performSelector:@selector(nextCredit) onTarget:self];
        
        SKAction *cycle = [SKAction sequence:@[fadeIn, delay, fadeOut, nextMessage]];
        
        SKAction *repeatCycle = [SKAction repeatActionForever:cycle];
        
        [_label runAction:repeatCycle completion:^{
            [self nextCredit];
        }];
    }
    return self;
}

- (void)nextCredit {
    self.counter++;
    if (self.counter == [self.messages count]) { self.counter = 0; }
    
    self.label.text = self.messages[self.counter];
    
}

#pragma mark - Touch Handlers

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *touchNode = [self nodeAtPoint:location];
    
    SKAction *pushDown = [SKAction scaleTo:0.8 duration:0.2];
    SKAction *click = [SKAction playSoundFileNamed:@"click.wav" waitForCompletion:YES];
    SKAction *pushUp = [SKAction scaleTo:1.0 duration:0.1];
    
    SKAction *clickAndUp = [SKAction group:@[click, pushUp]];
    SKAction *push = [SKAction sequence:@[pushDown, clickAndUp]];
    
    if ([touchNode.name isEqualToString:@"menu"]) {
        [touchNode runAction:push completion:^{
            [self.musicPlayer stop];
            SKScene *tutorialScene = [[RWMenuScene alloc] initWithSize:self.size];
            [self.view presentScene:tutorialScene];
        }];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}


@end
